import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import router from "./router";
import cors from "cors";

const app = express();
const corsOptions = {
    "origin": "http://localhost:3000",
    "methods":"GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
};

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({"extended":"true"}));
app.use(bodyParser.json());
app.use(morgan("dev"));

app.use("/api/v1", router);

export default app;

