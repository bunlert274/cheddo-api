export default async ({ baseMessage, expect }) =>
    Promise.resolve(!!baseMessage.match(expect) || false);
