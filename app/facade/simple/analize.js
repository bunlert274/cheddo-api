export default async ({data}) => {
    const serializer = [...new Set(data)].sort((a, b) => b - a);
    return Promise.resolve(
        serializer.map((x) => ({ [x]: serializer.filter((y) => x > y).length }))
    );
};
