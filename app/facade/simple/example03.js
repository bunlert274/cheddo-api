export default async (validate) => {
    const validator = (v) => {
        const x = v.replace(/\(\)|\[]|{}/, "");
        return x === v ? !v : validator(x);
    };
    return Promise.resolve(validator(validate));
};
