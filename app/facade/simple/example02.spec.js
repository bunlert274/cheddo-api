import matchString from "./example02";

describe("test simple case", () => {
    it("should be return true", async () => {
        const expected = true;
        const rs = await matchString({ baseMessage: "AbcDef", expect: "Ab"});
        expect(rs).toBe(expected);
    });

    it("should be return false 1", async () => {
        const expected = false;
        const rs = await matchString({ baseMessage: "AbcDef", expect: "Bc" });
        expect(rs).toBe(expected);
    });

    it("should be return false 2", async () => {
        const expected = false;
        const rs = await matchString({ baseMessage: "AbcDef", expect: "cd" });
        expect(rs).toBe(expected);
    });
});
