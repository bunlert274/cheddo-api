import getOne from "./analize";

describe("test simple case", () => {
    it("should be get data case 1", async () => {
        const expected = [{ "16": 4 }, { "9": 3 }, { "3": 2 }, { "1": 1 }, { "-1": 0 }];
        const rs = await getOne({ data: [9, 16, 3, 3, 9, 1, -1, -1] });
        expect(rs).toEqual(expected);
    });

    it("should be get data case 2", async () => {
        const expected = [{ "8": 4 }, { "4": 3 }, { "3": 2 }, { "2": 1 }, { "1": 0 }];
        const rs = await getOne({ data: [8, 4, 3, 1, 2, 2]});
        expect(rs).toEqual(expected);
    });

});
