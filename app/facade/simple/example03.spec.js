import validator from "./example03";

describe("test simple case", () => {
    it("should be return true example01", async () => {
        const expected = true;
        const rs = await validator("[]{}()");
        expect(rs).toBe(expected);
    });

    it("should be return false example02", async () => {
        const expected = false;
        const rs = await validator("({)}[]");
        expect(rs).toBe(expected);
    });

    it("should be return true example03", async () => {
        const expected = true;
        const rs = await validator("[{()}]");
        expect(rs).toBe(expected);
    });
});
