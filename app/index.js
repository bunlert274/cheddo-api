import logger from "winston";
import apps from "./server";
import { server } from "./configs";

apps.listen(server.port, () => {
    logger.info(`Server running at port ${server.port}`);
});