"use strict";
import mongoose from "mongoose";
import Model from "./models";

const setupModel = (connector, Model) => (obj, key) => {
    obj[key] = connector.model(key, Model[key]);
    return obj;
};

const getConnector = (db) => {
    return mongoose.createConnection(`mongodb://${db.host}:${db.port}/${db.database || undefined}`);
};

const getModel = (connector) => {
    return ["Members"]
        .reduce(setupModel(connector, Model), {});
};

export default { getConnector, getModel };