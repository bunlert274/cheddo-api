import crypto from "crypto";
import { Schema } from "mongoose";
import findOrCreate from "mongoose-findorcreate";

const MemberSchema = new Schema({
    fullname: String,
    email: String,
    username: {
        type: String,
        trim: true,
        unique: true
    },
    password: String,
    is_delete: {
        type: Boolean,
        default: false
    },
    is_approve: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "updated_at"
    }
});

MemberSchema.pre("save", function(next) {
    if(this.password) {
        const hash = crypto.createHash("md5");
        this.password = hash.update(this.password).digest("hex");
    }
    next();
});

MemberSchema.plugin(findOrCreate);
export default MemberSchema;