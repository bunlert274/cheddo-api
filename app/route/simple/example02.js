import { simple } from "../../facade";

export default ({ body }, res) => {
    simple
        .example02(body)
        .then((response) => res.status(200).json(response))
        .catch(() => res.status(500));
};
