import analize from "./analize";
import example02 from "./example02";
import example03 from "./example03";

module.exports = {
    analize,
    example02,
    example03
};