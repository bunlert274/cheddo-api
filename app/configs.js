"use strict";

const ENV = process.env.NODE_ENV || "development";
const configs  = require(`../configs/${ENV}`);

export const mongo = {
    host: configs.mongo.host,
    port: configs.mongo.port,
    database: configs.mongo.database
};

export const server = {
    port: process.env.PORT || configs.server.port
};