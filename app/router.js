import { Router } from "express";
import api from "./route";

const router = Router();

router.route("/example01")
    .post(api.simple.analize);

router.route("/example02")
    .post(api.simple.example02);

router.route("/example03")
    .post(api.simple.example03);

export default router;